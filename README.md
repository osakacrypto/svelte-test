# svelte-test

## @OsakaCrypto

This project showcases the following features:

- A search bar for the user to submit a query
- A results grid that displays matching images in a grid layout
- Basic pagination or infinite scrolling

- A way to display image metadata (e.g. the image name and location)
- Filters for the image grid
- A zoom control to change the size of images in the grid
- A way for the user to select multiple images in the grid to download

This project makes use of the NASA public API. <https://api.nasa.gov/>

This is a project built using the following stack:
Svelte,
Typescript,
Three.js,
Gitlab,
Hostinger,
API

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Running this project 🏃

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Authors and acknowledgment

@OsakaCrypto.
Shout out to the Svelte community for all the open source libraries / packages and contributions.

## License

This is open source. Please feel free to use, fork and contribute!

## Project status

Complete. Unlikely to involve further commits.
